#include <vector>
#include <iostream>

#include "solution.hh"

// Carpal tunnel avoider
using strbool = std::pair<std::string, bool>;

int main(void) {
    Solution sol;

    std::vector<strbool> testcases;
    testcases.push_back(strbool("LR", true));
    testcases.push_back(strbool("GRRGG", true));
    testcases.push_back(strbool("GRRGRR", true));
    testcases.push_back(strbool("GGGG", false));
    testcases.push_back(strbool("GGLLGG", true));
    testcases.push_back(strbool("GL", true));
    testcases.push_back(strbool("GLLL", true));
    testcases.push_back(strbool(
            "GLLLLRRRRLLLLRRRRLLLLRRRRLLLLRRRR", false));
    testcases.push_back(strbool(
            "LRLRLRLRLRLRLRLRLRLRLRGGGGGGGGGGGGGGGGGGGGGGGGL", true));
    testcases.push_back(strbool("LRRRRLRRLLRRR", true));

    std::cout << "Testing your solution for robot bounded in circle...\n";
    int i = 1;
    int f = 0;
    for (auto test : testcases) {
        bool response = sol.isRobotBounded(test.first);
        if (response != test.second) {
            ++f;
            std::cout << "Wrong answer " << (response ? "true" : "false")
                      << " for " << test.first << std::endl;
        }
        ++i;
    }
    if (f) {
        std::cout << "Failed (" << f << "/" << i << ") tests\n";
        return 0;
    }
    std::cout << "You passed all tests!\n";
    return 0;
}
